FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN pip3 install  flask flask-restful
EXPOSE 5000
CMD [ "python","dockerproject.py" ]
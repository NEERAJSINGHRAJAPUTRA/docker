# # using flask_restful
from flask import Flask
import math
from flask import Flask, jsonify, request
from flask_restful import Resource, Api

# # creating the flask app
# app = Flask(__name__)
# # creating an API object
# api = Api(app)

class Facto:
    def get(self, num):
        answer = math.factorial(num)
        return jsonify({'Facto': answer})

class Fibo:
     def get(self, num):
        a = 0
        b = 1
        result = 0
        count = 1
        result_string = "0"
        while count < num:
            count += 1
            a = b
            b = result
            result = a + b
            result_string += str(result)
        return jsonify({'Fibo': result_string})

# # adding the defined resources along with their corresponding urls
# api.add_resource(Factorial, '/Factorial/<int:num>')
# api.add_resource(Fibonacci, '/Fibonacci/<int:num>')

# # driver function
# if __name__ == '__main__':
#  app.run(host="127.0.0.1", port=5000, debug=True)
app = Flask(__name__)

@app.route('/facto/<int:num>', methods=['POST'])
def facto(num):
    number = Facto()
    res = number.get(num)
    return res
@app.route('/fibo/<int:num>', methods=['POST'])
def fibo(num):
    number = Fibo()
    res = number.get(num)
    return res
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
